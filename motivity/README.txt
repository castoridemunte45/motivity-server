motivity README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_motivity_db development.ini#motivity

- $VENV/bin/pserve development.ini

