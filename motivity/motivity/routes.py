from motivity import context


def convert_integers(*fields):
    """ Create a predicate for converting all specified fields to their equivalent integer values
    @param fields:  list of placeholder names to be converted to integer
    @return:        function performing conversion
    """
    def predicate(info, request):
        match = info['match']
        for arg in fields:
            if arg not in match:
                return False
            try:
                match[arg] = int(match[arg])
            except ValueError:
                return False
        return True
    return predicate


def includeme(config):
    config.add_route('auth.login', 'api/auth/login', factory=context.Everyone)
    config.add_route('auth.sync', 'api/sync', factory=context.Everyone)
    config.add_route('auth.logout', 'api/auth/logout', factory=context.Public)
    config.add_route('auth.register', 'api/auth/register', factory=context.Everyone)
    config.add_route('auth.me', 'api/auth/me', factory=context.Public)
    config.add_route('auth.check', 'api/auth/check', factory=context.Everyone)

    config.add_route('users', 'api/user', factory=context.Private)
    config.add_route('user', 'api/user/{id}', factory=context.Private,
                     custom_predicates=(convert_integers('id'),))
    config.add_route('user.self', 'api/update/{user_id}', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))
    config.add_route('user.stats', 'api/stats/{user_id}', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))

    config.add_route('achievements', 'api/achievement', factory=context.Public)
    config.add_route('achievement', 'api/achievement/{id}', factory=context.Public,
                     custom_predicates=(convert_integers('id'),))

    config.add_route('user.friends', 'api/user/{user_id}/friend', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))
    config.add_route('user.friend', 'api/user/{user_id}/friend/{id}', factory=context.Self,
                     custom_predicates=(convert_integers('id', 'user_id'),))

    config.add_route('user.achievements', 'api/user/{user_id}/achievement', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))
    config.add_route('user.achievement', 'api/user/{user_id}/achievement/{id}', factory=context.Self,
                     custom_predicates=(convert_integers('id', 'user_id'),))

    config.add_route('user.missions', 'api/user/{user_id}/mission', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))
    config.add_route('user.mission', 'api/user/{user_id}/mission/{id}', factory=context.Self,
                     custom_predicates=(convert_integers('id', 'user_id'),))

    config.add_route('activities', 'api/user/{user_id}/activity', factory=context.Self,
                     custom_predicates=(convert_integers('user_id'),))
    config.add_route('activity', 'api/user/{user_id}/activity/{id}', factory=context.Self,
                     custom_predicates=(convert_integers('id','user_id'),))

