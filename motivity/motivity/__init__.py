from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from .models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    config = Configurator(settings=settings)

    authentication_policy = AuthTktAuthenticationPolicy("4V3ry$3cr3tK3y", hashalg="sha512",
                                                        include_ip=True, debug=True, callback=get_user_permissions)
    authorization_policy = ACLAuthorizationPolicy()
    config.set_authentication_policy(authentication_policy)
    config.set_authorization_policy(authorization_policy)

    from .json import json_renderer
    config.add_renderer('json', json_renderer)

    config.include('pyramid_jinja2')
    config.include('.routes')
    config.scan()
    return config.make_wsgi_app()


def get_user_permissions(user_recv, request):
    from .models import DBSession, user
    from sqlalchemy.orm.exc import NoResultFound

    if type(user_recv) is not int:
        return None

    user_obj = getattr(request, "user_object", None)
    if user_obj is None:
        try:
            user_obj = DBSession.query(user.User).get(user_recv)
            request.user_object = user_obj
        except NoResultFound:
            return None

    if user_obj.is_admin is True:
        return ['admin', user_obj.email, user_obj.id]
    if user_obj.is_admin is False:
        return ["user", user_obj.email, user_obj.id]