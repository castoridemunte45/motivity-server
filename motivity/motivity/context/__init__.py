from pyramid.security import Allow


class Public:
    __acl__ = [
        (Allow, "admin", ("search", "list", "get", "add", "update", "delete", "lookup", "email")),
        (Allow, "user", ("search", 'list', "get", 'add', 'update', 'delete', "lookup", "email")),
    ]

    def __init__(self, request):
        pass


class Protected:
    __acl__ = [
        (Allow, "admin", ("search", "list", "get", "add", "update", "delete", "lookup", "email")),
        (Allow, "user", ("search", 'list', "get", "lookup")),
    ]

    def __init__(self, request):
        pass


class Private:
    __acl__ = [
        (Allow, "admin", ("search", "list", "get", "add", "update", "delete", "lookup")),
    ]

    def __init__(self, request):
        pass


class Everyone:
    __acl__ = [
        (Allow, "system.Everyone", ("search", "list", "get", "add", "update", "delete", "lookup")),
    ]

    def __init__(self, request):
        pass


class Self:
    def __init__(self, request):
        self.id = request.matchdict['user_id']
        self.__acl__ = [
            (Allow, "admin", ("search", "list", "get", "add", "update", "delete", "lookup")),
            (Allow, self.id, ("search", 'list', "get", 'add', 'update', 'delete', "lookup", "email")),
        ]

