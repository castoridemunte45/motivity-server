import pyramid
from pyramid.view import view_config
from pyramid.request import Request
from pyramid.httpexceptions import HTTPNotFound, HTTPServerError, HTTPOk, HTTPConflict, HTTPUnauthorized, HTTPForbidden
from sqlalchemy.orm.exc import NoResultFound, StaleDataError
from sqlalchemy.orm.util import AliasedClass

from ..models import DBSession
from ..json import VersionCheckError

import transaction


# @view_config(context=Exception)
# def error_view(e, request):
#     import traceback
#     ret = pyramid.response.Response(
#         "500 internal error\n"+
#         traceback.format_exc(), 500, content_type="text/plain", charset="utf-8")
#     return ret


class CRUDView(object):
    filters = dict()
    accept_order = dict()
    json_hints = dict()
    update_hints = dict()
    context = None
    target_type = object
    target_name = "object"
    request = None
    update_lock = False
    # this means accept_order will be rewritten
    use_subquery_after_filter = False

    def __init__(self, request):
        """:type request: Request"""
        self.request = request
        self.context = request.context
        self.context._json_hints = self.json_hints
        assert(isinstance(self.request, Request))

    @property
    def identity_filter(self):
        return False

    def sanitize_input(self):
        return self.request.json_body

    @property
    def context_filter(self):
        return True

    def get_by_id(self, update_lock=False):
        try:
            query = self.get_identity_base()
            if update_lock:
                # (partially a) SQLAlchemy bug: Postgres doesn't allow qualified table names in
                # FOR UPDATE OF statements, so whenever targeted locks are required an alias is necessary,
                # for us at least since we use schemas for all tables
                if isinstance(self.target_type, AliasedClass):
                    query = query.with_for_update(of=self.target_type)
                else:
                    query = query.with_for_update()
            return query.filter(self.context_filter, self.identity_filter).one()
        except NoResultFound:
            raise HTTPNotFound()

    @property
    def query_filters(self):
        tmp = [self.filters[key](self.request.GET[key]) for key in self.filters
               if key in self.request.GET]
        return [i for i in tmp if not isinstance(i,(list,tuple)) and i is not None] + \
               [i for j in tmp if isinstance(j, (list,tuple)) and j is not None for i in j if i is not None]

    @property
    def order_clauses(self):
        if 'order' not in self.request.GET or self.request.GET['order'] == "":
            return [False]
        orders = [(item[:-5], True) if item.endswith(' desc') else (item, False)
                  for item in self.request.GET['order'].split(",")]
        if any([order[0] not in self.accept_order for order in orders]):
            raise HTTPServerError("not implemented, order by " +
                                  ", ".join([order[0] for order in orders if order[0] not in self.accept_order]))
        # return [self.accept_order[order[0]].desc() if order[1] else self.accept_order[order[0]] for order in orders]
        selected = [(self.accept_order[order[0]], order[1]) for order in orders]
        for item in selected:
            if isinstance(item[0], list):
                for elem in item[0]:
                    yield elem.desc() if item[1] else elem
            else:
                yield item[0].desc() if item[1] else item[0]

    @property
    def pager_slice(self):
        if 'pageSize' not in self.request.GET:
            return False
        page_size = int(self.request.GET['pageSize'])
        if page_size <= 0:
            return False
        page = int(self.request.GET['page']) if 'page' in self.request.GET else 0
        return slice(page * page_size, page * page_size + page_size)

    def get_base_query(self):
        return DBSession.query(self.target_type)

    def get_identity_base(self):
        return self.get_base_query()

    def get_list_base(self):
        return self.get_base_query()

    def get_search_results(self, query=None):
        if query is None:
            query = self.get_list_base().filter(self.context_filter)
        query = query.filter(*self.query_filters)
        pager = self.pager_slice
        count = query.count()

        if self.use_subquery_after_filter:
            query = query.subquery()
            self.accept_order = dict(query.c)
            query = DBSession.query(*query.c).order_by(*self.order_clauses)
        else:
            query = query.order_by(*self.order_clauses)
        return query[pager] if pager else query.all(), count

    def get(self):
        return {self.target_name: self.get_by_id()}

    def list(self):
        items, count = self.get_search_results()
        return dict(items=[item._asdict() if isinstance(item, tuple) and callable(getattr(item, '_asdict', None))
                           else item for item in items], total=count)

    def apply_changes(self, obj, data, for_update=True):
        obj.update_conditional(data, self.update_hints, ignore_invalid=True)

    def update(self):
        try:
            with transaction.manager, DBSession.no_autoflush:
                old = self.get_by_id(update_lock=self.update_lock)
                values = self.sanitize_input()
                try:
                    self.apply_changes(old, values, True)
                except VersionCheckError as ex:
                    raise HTTPConflict(str(ex))
        except StaleDataError as ex:
            raise HTTPConflict(str(ex))
        return self.get()

    def insert(self):
        obj = self.target_type()
        values = self.sanitize_input()
        with transaction.manager:
            self.apply_changes(obj, values, False)
            DBSession.add(obj)
        obj = DBSession.merge(obj)
        return {self.target_name: obj}

    def delete(self):
        with transaction.manager:
            old = self.get_by_id()
            DBSession.delete(old)
        return HTTPOk()