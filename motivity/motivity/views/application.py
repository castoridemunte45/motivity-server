from pyramid.view import view_config, view_defaults
from sqlalchemy import or_

from ..models import DBSession
from ..models.application import Mission, Activity
from . import CRUDView


@view_defaults(renderer='json')
@view_config(route_name='user.mission', request_method='GET', attr='get', permission='get')
@view_config(route_name='user.missions', request_method='GET', attr='list', permission='list')
@view_config(route_name='user.mission', request_method='PUT', attr='update', permission='update')
@view_config(route_name='user.missions', request_method='POST', attr='insert', permission='add')
@view_config(route_name='user.mission', request_method='DELETE', attr='delete', permission='delete')
class MissionView(CRUDView):
    filters = dict(keyword=lambda val: or_(Mission.title.ilike("%"+val+"%"), Mission.description.ilike("%"+val+"%")),
                   activity_id=lambda val: Mission.activity_id == int(val),
                   to_duration=lambda val: Mission.duration <= int(val),
                   from_duration=lambda val: Mission.duration >= int(val),
                   completed=lambda val: Mission.completed == bool(val),
                   to_progress=lambda val: Mission.progress <= int(val),
                   from_progress=lambda val: Mission.progress >= int(val),
                   to_milestone=lambda val: Mission.milestone <= int(val),
                   from_milestone=lambda val: Mission.milestone >= int(val)
                   )

    accept_order = dict(duration=Mission.duration,
                        milestone=Mission.milestone
                        )

    update_hints = dict()
    json_hints = dict(activities=True, users=True)
    target_type = Mission
    target_name = "mission"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']
        if 'user_id' in self.request.matchdict:
            self.user_id = self.request.matchdict['user_id']

    @property
    def identity_filter(self):
        return Mission.id == self.id

    def sanitize_input(self):
        return self.request.json_body['mission']

    def apply_changes(self, obj, data, for_update=True):
        data["user_id"] = self.user_id
        if for_update and 'progress' in data and int(data["progress"]) >= obj.milestone:
            data["completed"] = True
        obj.update_conditional(data, dict(_exclude_=['id', 'activities', 'user', 'created_at']), ignore_invalid=True)

        if 'activityIds' in data:
            by_pk = {item for item in data['activityIds'] if item is not None}
            to_remove = [item for item in obj.activities if item.id not in by_pk]
            existing_by_pk = {item.id: item for item in obj.activities}

            # delete if no longer exists
            for item in to_remove:
                obj.activities.remove(item)

            # add new
            for item_data in data['activityIds']:
                if item_data not in existing_by_pk:
                    item = DBSession.query(Activity).filter_by(id=item_data).one()
                    obj.activities.append(item)

        if for_update and obj.completed and 'activities' in data:
            by_pk = {item['id']: item for item in data['activities']
                     if item is not None and item['id'] is not None}
            to_remove = [item for item in obj.activities if item.id not in by_pk]
            existing_by_pk = {item.id: item for item in obj.activities}

            # delete if no longer exists
            for item in to_remove:
                obj.activities.remove(item)

            # update
            for item_data in data['activities']:
                item = existing_by_pk[item_data['id']]
                item.update_conditional(item_data, ignore_invalid=True, hints=dict(
                    _exclude_=['id', 'user', 'mission']
                ))

    def get_list_base(self):
        return DBSession.query(Mission).filter_by(user_id=self.user_id).order_by(Mission.created_at.desc())


@view_defaults(renderer='json')
@view_config(route_name='activity', request_method='GET', attr='get', permission='get')
@view_config(route_name='activities', request_method='GET', attr='list', permission='list')
@view_config(route_name='activity', request_method='PUT', attr='update', permission='update')
@view_config(route_name='activities', request_method='POST', attr='insert', permission='add')
@view_config(route_name='activity', request_method='DELETE', attr='delete', permission='delete')
class ActivityView(CRUDView):
    filters = dict(
        keyword=lambda val: or_(Activity.title.ilike("%" + val + "%"),
                                Activity.description.ilike("%" + val + "%")),
        user_id=lambda val: Activity.user_id == int(val),
        to_difficulty=lambda val: Activity.difficulty <= int(val),
        from_difficulty=lambda val: Activity.difficulty >= int(val),
        to_experience=lambda val: Activity.experience <= int(val),
        from_experience=lambda val: Activity.experience >= int(val),
    )

    accept_order = dict(title=Activity.title
                        )

    update_hints = dict()
    json_hints = dict(user=dict(password=False))
    target_type = Activity
    target_name = "activity"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']
        if 'user_id' in self.request.matchdict:
            self.user_id = self.request.matchdict['user_id']

    @property
    def identity_filter(self):
        return Activity.id == self.id

    def sanitize_input(self):
        return self.request.json_body['activity']

    def apply_changes(self, obj, data, for_update=True):
        obj.update_conditional(data, dict(_exclude_=['id', 'user', 'mission']), ignore_invalid=True)

    def get_list_base(self):
        return DBSession.query(Activity).filter_by(user_id=self.user_id)