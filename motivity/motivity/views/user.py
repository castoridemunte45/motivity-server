import dateutil.parser

from pyramid.view import view_config, view_defaults
from sqlalchemy import or_, func

from ..models import DBSession
from ..models.user import User, Achievement, Friend, UserAchievement
from ..models.application import Mission, Activity
from . import CRUDView


def crypt_password_simple(password):
    if password == '' or password is None:
        return None
    import uuid, hashlib
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ":" + salt


def verify_password(clear_text, encrypted, accept_clear_text=False):
    if accept_clear_text and clear_text == encrypted: return True
    import hashlib
    salt = encrypted.split(':')[1]
    to_check = hashlib.sha256(salt.encode() + clear_text.encode()).hexdigest() + ':' + salt
    return encrypted == to_check


@view_defaults(renderer='json')
@view_config(route_name='auth.register', request_method='POST', attr='insert', permission='add')
@view_config(route_name='user.self', request_method='PUT', attr='update', permission='update')
@view_config(route_name='user.self', request_method='GET', attr='get', permission='get')
@view_config(route_name='user', request_method='GET', attr='get', permission='get')
@view_config(route_name='users', request_method='GET', attr='list', permission='list')
@view_config(route_name='user', request_method='PUT', attr='update', permission='update')
@view_config(route_name='users', request_method='POST', attr='insert', permission='add')
@view_config(route_name='user', request_method='DELETE', attr='delete', permission='delete')
@view_config(route_name='user.stats', request_method='GET', attr='stats', permission='list')
class UserView(CRUDView):
    filters = dict(name=lambda val: or_(User.first_name == val, User.last_name == val),
                   name_like=lambda val: or_(User.first_name.ilike("%"+val+"%"), User.last_name.ilike("%"+val+"%")),
                   email=lambda val: User.email == val,
                   email_like=lambda val: User.email.ilike("%" + val + "%"),
                   )

    accept_order = dict(email=User.email,
                        first_name=User.first_name,
                        last_name=User.last_name
                        )

    update_hints = dict()
    json_hints = dict(password=False, user_achievements=dict(achievement=True))
    target_type = User
    target_name = "user"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']
        if 'id' not in self.request.matchdict and 'user_id' in self.request.matchdict:
            self.id = self.request.matchdict['user_id']

    @property
    def identity_filter(self):
        return User.id == self.id

    def sanitize_input(self):
        return self.request.json_body['user']

    def apply_changes(self, obj, data, for_update=True):
        obj.update_conditional(data,
                               dict(_exclude_=['id', 'user_achievements'], password=crypt_password_simple),
                               ignore_invalid=True)

        if not for_update:
            achievements = DBSession.query(Achievement).all()
            for j in range(0, len(achievements)):
                obj.user_achievements.append(
                    UserAchievement(achievement_id=achievements[j].id, user_id=obj.id, progress=0))

    def get_list_base(self):
        return DBSession.query(User)

    def stats(self):
        completed_missions = len(DBSession.query(Mission).filter_by(user_id=self.request.matchdict['user_id'])
                                 .filter_by(completed=True).all())
        activities = len(DBSession.query(Activity).filter_by(user_id=self.request.matchdict['user_id']).all())
        friends = len(DBSession.query(Friend).filter_by(user_id=self.request.matchdict['user_id']).all())
        return { "completed_missions": completed_missions, "activities": activities, "friends": friends }


@view_defaults(renderer='json')
@view_config(route_name='achievement', request_method='GET', attr='get', permission='get')
@view_config(route_name='achievements', request_method='GET', attr='list', permission='list')
@view_config(route_name='achievement', request_method='PUT', attr='update', permission='update')
@view_config(route_name='achievements', request_method='POST', attr='insert', permission='add')
@view_config(route_name='achievement', request_method='DELETE', attr='delete', permission='delete')
class AchievementView(CRUDView):
    filters = dict(keyword=lambda val: or_(Achievement.title.ilike("%"+val+"%"), Achievement.description.ilike("%"+val+"%")),
                   to_milestone=lambda val: Achievement.milestone <= int(val),
                   from_milestone=lambda val: Achievement.milestone >= int(val),
                   target=lambda val: Achievement.target == val
                   )

    accept_order = dict(target = Achievement.target,
                        title = Achievement.title
                        )

    update_hints = dict()
    json_hints = dict(user_achievements=True)
    target_type = Achievement
    target_name = "achievement"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']

    @property
    def identity_filter(self):
        return Achievement.id == self.id

    def sanitize_input(self):
        return self.request.json_body['achievement']

    def apply_changes(self, obj, data, for_update=True):
        obj.update_conditional(data, dict(_exclude_=['id', 'user_achievements']), ignore_invalid=True)

    def get_list_base(self):
        return DBSession.query(Achievement)


@view_defaults(renderer='json')
@view_config(route_name='user.friend', request_method='GET', attr='get', permission='get')
@view_config(route_name='user.friends', request_method='GET', attr='list', permission='list')
@view_config(route_name='user.friend', request_method='PUT', attr='update', permission='update')
@view_config(route_name='user.friends', request_method='POST', attr='insert', permission='add')
@view_config(route_name='user.friend', request_method='DELETE', attr='delete', permission='delete')
class FriendView(CRUDView):
    filters = dict(nickname=lambda val: Friend.nickname.ilike("%" + val + "%"),)

    accept_order = dict(nickname=Friend.nickname)

    update_hints = dict()
    json_hints = dict()
    target_type = Friend
    target_name = "friend"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']
        if 'user_id' in self.request.matchdict:
            self.user_id = self.request.matchdict['user_id']

    @property
    def identity_filter(self):
        return Friend.id == self.id

    def sanitize_input(self):
        return self.request.json_body['friend']

    def apply_changes(self, obj, data, for_update=True):
        data['user_id'] = self.user_id
        exclude=[]
        if for_update is True:
            exclude=['id', 'user_id', 'friend_id','user', 'friend']
        else:
            exclude = ['id', 'user', 'friend']
        obj.update_conditional(data, dict(_exclude_=exclude), ignore_invalid=True)

    def get_list_base(self):
        return DBSession.query(Friend).filter_by(user_id=self.user_id)


@view_defaults(renderer='json')
@view_config(route_name='user.achievement', request_method='GET', attr='get', permission='get')
@view_config(route_name='user.achievements', request_method='GET', attr='list', permission='list')
@view_config(route_name='user.achievement', request_method='PUT', attr='update', permission='update')
@view_config(route_name='user.achievements', request_method='POST', attr='insert', permission='add')
@view_config(route_name='user.achievement', request_method='DELETE', attr='delete', permission='delete')
class UserAchievementView(CRUDView):
    filters = dict(to_progress=lambda val: UserAchievement.progress <= int(val),
                   completed=lambda val: UserAchievement.completed == bool(val),
                   date_from=lambda val: UserAchievement.date_acquired >= dateutil.parser.parse(val).date(),
                   date_to=lambda val: UserAchievement.date_acquired <= dateutil.parser.parse(val).date()
                   )

    accept_order = dict(progress=UserAchievement.progress,
                        date_acquired=UserAchievement.date_acquired
                        )

    update_hints = dict()
    json_hints = dict(achievement=True)
    target_type = UserAchievement
    target_name = "user_achievement"

    def __init__(self, request):
        super().__init__(request)
        if 'id' in self.request.matchdict:
            self.id = self.request.matchdict['id']
        if 'user_id' in self.request.matchdict:
            self.user_id = self.request.matchdict['user_id']

    @property
    def identity_filter(self):
        return UserAchievement.id == self.id

    def sanitize_input(self):
        return self.request.json_body['user_achievement']

    def apply_changes(self, obj, data, for_update=True):
        data['user_id'] = self.user_id
        exclude = []
        if for_update is True:
            if obj.completed is False:
                obj.completed = True
                obj.date_acquired = func.now()
            exclude = ['id', 'user_id', 'achievement_id', 'user', 'achievement']
        else:
            exclude = ['id', 'user', 'achievement']
        obj.update_conditional(data, dict(_exclude_=exclude), ignore_invalid=True)

    def get_list_base(self):
        return DBSession.query(UserAchievement).filter_by(user_id=self.user_id)