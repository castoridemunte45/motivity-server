from pyramid.httpexceptions import HTTPForbidden, HTTPOk
from pyramid.request import Request, Response
from pyramid.view import view_config
from sqlalchemy.orm.exc import NoResultFound
from pyramid.security import remember, forget, unauthenticated_userid

from ..models import DBSession, user, application
from ..views.user import verify_password

json_hints = dict(
    user_achievements=dict(achievement=True),
    _exclude_=['password'],
)


@view_config(route_name="auth.login", request_method='POST', renderer="json")
def auth_login(request):
    assert(isinstance(request, Request))
    assert(isinstance(request.response, Response))
    json = request.json_body["credentials"]

    try:
        user_obj = DBSession.query(user.User) \
            .filter_by(email=json['email'])\
            .one()
    except NoResultFound:
        return HTTPForbidden("login failed")

    # check password
    if not verify_password(clear_text=json['password'], encrypted=user_obj.password, accept_clear_text=True):
        return HTTPForbidden("login failed")

    request.response.headerlist.extend(remember(request, user_obj.id))
    request.context._json_hints = json_hints
    return {'user': user_obj}


@view_config(route_name="auth.logout", request_method='GET', renderer="json")
def auth_logout(request):
    assert(isinstance(request, Request))
    request.response.headerlist.extend(forget(request))
    return HTTPOk('Logged out', headers=request.response.headers)


@view_config(route_name="auth.me", request_method='GET', renderer="json")
def auth_me(request):
    assert(isinstance(request, Request))
    user_id = unauthenticated_userid(request)
    if user_id is not None:
        request.context._json_hints = json_hints
        user_obj = DBSession.query(user.User) \
            .filter_by(id=user_id) \
            .one()
        return user_obj
    else:
        return HTTPForbidden()


@view_config(route_name="auth.sync", request_method='POST', renderer="json")
def auth_sync(request):
    assert(isinstance(request, Request))
    assert(isinstance(request.response, Response))
    json = request.json_body["credentials"]

    try:
        user_obj = DBSession.query(user.User) \
            .filter_by(email=json['email'])\
            .one()
    except NoResultFound:
        return HTTPForbidden("login failed")

    # check password
    if not verify_password(clear_text=json['password'], encrypted=user_obj.password, accept_clear_text=True):
        return HTTPForbidden("login failed")

    activities = DBSession.query(application.Activity).filter_by(user_id=user_obj.id).all()
    # request.context._json_hints = json_hints
    return {'activities': activities}


@view_config(route_name="auth.check", request_method='GET', renderer="json")
def auth_check(request):
    return HTTPOk('')