from datetime import datetime, date, tzinfo, time
from decimal import Decimal
from enum import Enum

from pyramid.renderers import JSON
from sqlalchemy.orm import RelationshipProperty, ColumnProperty, CompositeProperty
from sqlalchemy.orm.interfaces import MapperProperty
from sqlalchemy import DateTime, Time, String
from dateutil import parser, tz
from sqlalchemy.inspection import inspect
import postgresql.types

from . import Base
from .models.enum import EnumSymbol, DeclEnum


def datetime_adapter(obj, request):
    if isinstance(obj, datetime):
        if obj.tzinfo is None:
            return obj.isoformat()
        if hasattr(request.context, 'timezone') and \
                isinstance(request.context.timezone, tzinfo):
            return obj.astimezone(request.context.timezone).isoformat()
        return obj.astimezone(tz.tzlocal()).isoformat()
    if hasattr(obj, "isoformat"):
        formatter = getattr(obj, "isoformat")
        if callable(formatter):
            return formatter()
    raise TypeError(obj)

def EnumSymbol_adapter(obj, request):
    if isinstance(obj, EnumSymbol):
        return obj.value if obj.cls._json_use_values else obj.name
    raise TypeError(obj)

def Decimal_adapter(obj, request):
    if (isinstance(obj, Decimal)):
        return float(obj)
    raise TypeError(obj)

def Enum_adapter(obj, request):
    if getattr(type(obj), "_json_use_values", False):
        return obj.value
    return obj.name

def postgresql_array_adapter(obj, request):
    return list(obj)

import suds.sudsobject, suds.sax.text, suds.sax.date
def sudsobject_addapter(obj, request):
    return {key: getattr(obj, key) for key in obj.__keylist__}

# this helper will go through a structure received from suds (webservice) and try its best to convert it to standard
# dictionary form compatible
def sudsobject_todict(obj):
    if isinstance(obj, suds.sudsobject.Object):
        ret = dict()
        for key in obj.__keylist__:
            val = getattr(obj, key)
            val = sudsobject_todict(val)
            ret[key] = val
        return ret
    if isinstance(obj, list):
        return [sudsobject_todict(item) for item in obj]
    if isinstance(obj, suds.sax.text.Text):
        return str(obj)
    if isinstance(obj, datetime) and obj.tzinfo is not None \
            and isinstance(obj.tzinfo, (suds.sax.date.FixedOffsetTimezone,
                                        suds.sax.date.LocalTimezone,
                                        suds.sax.date.UtcTimezone)):
        return obj.astimezone(tz.tzlocal())
    return obj

json_renderer = JSON(indent=4)
json_renderer.add_adapter(datetime, datetime_adapter)
json_renderer.add_adapter(date, datetime_adapter)
json_renderer.add_adapter(EnumSymbol, EnumSymbol_adapter)
json_renderer.add_adapter(Enum, Enum_adapter)
json_renderer.add_adapter(Decimal, Decimal_adapter)
json_renderer.add_adapter(postgresql.types.Array, postgresql_array_adapter)
json_renderer.add_adapter(suds.sudsobject.Object, sudsobject_addapter)


class DictSerializable(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


    def __json__(self, request):
        hints = request.context._json_hints if hasattr(request.context, '_json_hints') \
            else None
        return self.__serialize_conditional(hints)

    def __serialize_conditional(self, hints=None):
        assert isinstance(self, Base)
        if not hints or hints is True:
            hints = {}

        explicit = False
        if isinstance(hints, list):
            explicit = True
            hints = {col: True for col in hints}
        assert isinstance(hints, dict)

        if "_include_" in hints:
            if hints['_include_'] is True:
                explicit = True
            if isinstance(hints['_include_'], dict):
                explicit = True
                hints.update(hints['_include_'])
            if isinstance(hints['_include_'], list):
                explicit = True
                hints.update({col: True for col in hints['_include_']})

        if "_exclude_" in hints:
            assert isinstance(hints['_exclude_'], list)
            hints.update({col:False for col in hints['_exclude_']})

        ret = dict()
        mapper = inspect(self).mapper
        composite_members = [prop for comp in mapper.composites for prop in comp.props]

        for prop in mapper.iterate_properties:
            # explicit mode: column must be specified, else skip
            name = prop.key
            if explicit and not name in hints:
                continue

            if not explicit and prop in composite_members:
                continue

            # column expressly excluded from serialization
            if name in hints and (hints[name] is None or hints[name] is False):
                continue

            hint = hints.get(name, None)
            #prop = self.__mapper__._props[name]
            if isinstance(prop, RelationshipProperty):
                # rather than basing our assumptions on relationship laziness, query hint
                if hint is None: continue
            value = getattr(self, name)
            # json_out hint in mapper definition
            # TODO investigate how this can be better used on relationship properties, e.g. lists of values
            # TODO and dictionaries; see callable hints for an example
            if prop.info is not None and "json_out" in prop.info and callable(prop.info["json_out"]):
                value = prop.info["json_out"](value)
            elif hint is not None and callable(hint) and not isinstance(value, dict) and \
                    not (isinstance(prop, RelationshipProperty) and prop.uselist):
                # dictionaries and relations are treated separately
                value = hint(value)
            if prop.info is not None and 'json_name' in prop.info:
                name = prop.info['json_name']

            if isinstance(prop, RelationshipProperty) and prop.uselist:
                assert value is None or isinstance(value, list)
                if value is None:
                    ret[name] = None
                    continue
                ret[name] = list()
                for item in value:
                    if callable(hint):
                        ret[name]=hint(value)
                    elif isinstance(item, DictSerializable):
                        ret[name].append(item.__serialize_conditional(hint))
                    else:
                        ret[name].append(item)
                continue
            if isinstance(value, dict):
                ret[name] = dict()
                for key, val in value:
                    if callable(hint):
                        ret[name]=hint(value)
                    elif isinstance(val, DictSerializable):
                        ret[name][key] = val.__serialize_conditional(hint)
                    else:
                        ret[name][key] = val
            if isinstance(value, DictSerializable):
                ret[name] = value.__serialize_conditional(hint)
                continue
            ret[name] = value
        return ret

    @classmethod
    def __create_conditional(cls, source: dict, hints=None):
        print(cls,source,hints)
        pass

    def __coerce_value(self, prop, value, hint=None):
        assert isinstance(prop, ColumnProperty)
        column = prop.columns[0]
        # all checks are off for callable hints and decorations
        if column.primary_key and hint is None:
            raise PermissionError("update of primary key field %s for %r not allowed" %
                                  (prop.key, self.__class__))
        if not callable(hint) and 'json_in' in prop.info and callable(prop.info['json_in']):
            value = prop.info['json_in'](value)
        elif value is not None:
            python_type = column.type.python_type
            if python_type is str:
                if value is not str:
                    value = str(value)
                if isinstance(column.type, String) and hasattr(column.type, "length") \
                        and value is not None and column.type.length is not None \
                        and column.type.length < len(value):
                    raise ValueError("Text value too large for property %s of class %r, limit %d" %
                                     (prop.key, self.__class__, column.type.length))

            if python_type is int:
                try:
                    value=int(value)
                except ValueError:
                    raise ValueError("Could not convert value to target type for property %s of class %r" %
                                     (prop.key, self.__class__))

            if python_type is Decimal and value is not None and type(value) is not Decimal:
                try:
                    value = Decimal(value)
                except:
                    raise ValueError("Could not convert value to target type for property %s of class %r" %
                                     (prop.key, self.__class__))

            if python_type is float and value is not None and type(value) is not float:
                try:
                    value = float(value)
                except:
                    raise ValueError("Could not convert value to target type for property %s of class %r" %
                                     (prop.key, self.__class__))

            if python_type is bool and value is not None and type(value) is not bool:
                try:
                    if type(value) is str:
                        import distutils.util
                        value = bool(distutils.util.strtobool(value))
                    elif type(value) is int:
                        if value not in [0,1]:
                            raise ValueError
                        value = bool(value)
                    else:
                        raise ValueError
                except ValueError:
                    raise ValueError("Expected boolean value for property %s of class %r, received %r instead" %
                                     (prop.key, self.__class__, type(value)))

            if python_type in (datetime, date, time):
                try:

                    use_timezone = False
                    if isinstance(column.type, DateTime) or isinstance(column.type, Time):
                        use_timezone = column.type.timezone
                    value = parser.parse(value, tzinfos=lambda name, offset: tz.tzoffset(name, offset) \
                        if offset is not None else tz.gettz(name) or None, ignoretz=not use_timezone)
                    if python_type is date:
                        value = value.date()
                    if python_type is time:
                        value = value.timetz() if use_timezone else value.time()
                except ValueError:
                    raise ValueError("Could not convert value to target type for property %s of class %r" %
                                     (prop.key, self.__class__))

            if issubclass(python_type, DeclEnum) and value is not None:
                # there is no way that JSON knows about our enum types
                value = str(value)
                if value not in python_type.json_names():
                    raise ValueError(
                        "Could not convert value to target type for property %s of class %r; allowed enum values: %r, received value %s" %
                                     (prop.key, self.__class__, list(python_type.json_names()), value))
                value = python_type.from_string(value)

            if issubclass(python_type, Enum) and value is not None:
                value = str(value)
                value = next(item for item in python_type if item.name == value or item.value == value)

        if not column.nullable and value is None:
            raise ValueError("Null value not allowed for property %s of type %r" %
                             (prop.key, self.__class__))
        return value

    def update_conditional(self, source: dict, hints=None, ignore_invalid=False):
        assert isinstance(self, Base)
        assert source is not None
        assert isinstance(source, dict)
        if hints is None:
            hints = {}

        explicit = False
        if isinstance(hints, list):
            explicit = True
            hints = {col: True for col in hints}
        assert isinstance(hints, dict)

        if "_include_" in hints:
            if hints['_include_'] is True:
                explicit = True
            if isinstance(hints['_include_'], dict):
                explicit = True
                hints.update(hints['_include_'])
            if isinstance(hints['_include_'], list):
                explicit = True
                hints.update({col: True for col in hints['_include_']})

        if "_exclude_" in hints:
            assert isinstance(hints['_exclude_'], list)
            hints.update({col:False for col in hints['_exclude_']})

        mapper = inspect(self).mapper
        json_redirect = {prop.info['json_name']: prop.key for prop in mapper.iterate_properties if prop.info is not None
                    and 'json_name' in prop.info}

        for name in source:
            value = source[name]
            if name in json_redirect:
                name = json_redirect[name]
            if not mapper.has_property(name):
                if not ignore_invalid:
                    raise PermissionError("Mapped field %s not found in class %r" % (name, self.__class__))
                continue

            prop = mapper.get_property(name)
            assert isinstance(prop, MapperProperty)

            # update forbidden: explicit mode and column not specified or column blacklisted
            if (explicit and not name in hints) or (name in hints and (hints[name] is None or hints[name] is False)):
                if not ignore_invalid:
                    raise PermissionError("update of field %s for class %r is not allowed" %
                                          (name, self.__class__))
                continue

            hint = hints.get(name, None)

            if isinstance(prop, RelationshipProperty):
                if hint is None:
                    raise PermissionError("update of field %s for class %r is not allowed" %
                                      (name, self.__class__))
                raise NotImplemented("following relations not yet implemented: field %s for class %r" %
                                     (name, self.__class__))

            if isinstance(prop, ColumnProperty):
                if mapper.version_id_col is not None and mapper.version_id_col in prop.columns:
                    old_val = prop.class_attribute.__get__(self, None)
                    # pretty dirty hack -- for new instances the version is supposed to be null (but not all the time,
                    #   I believe the instance of the
                    if old_val is None:
                        continue
                    new_val = self.__coerce_value(prop, value, hint)
                    if new_val!=old_val:
                        raise VersionCheckError("Version check failure: database value is %r, JSON decoded value is %r"
                                                % (old_val, new_val))
                else:
                    # we need to compute return value here for lambda hint to bypass update
                    # and yes this is ugly
                    if callable(hint):
                        value = hint(value)
                        if value is None:
                            continue
                    prop.class_attribute.__set__(self, self.__coerce_value(prop, value, hint))

            if isinstance(prop, CompositeProperty):
                if value is None:
                    raise ValueError("update of composite field with null value not allowed")
                # for composites, leave it to the MutableComposite implementation to deserialize
                setattr(self, name, value)

        #    if isinstance(value, dict):
        #        ret[name] = dict()
        #        for key, val in value:
        #            if callable(hint):
        #                ret[name]=hint(value)
        #            elif isinstance(val, DictSerializable):
        #                ret[name][key] = val.SerializeConditional(hint)
        #            else:
        #                ret[name][key] = val
        #    if isinstance(value, DictSerializable):
        #        ret[name] = value.SerializeConditional(hint)
        #        continue
        #    ret[name] = value
        #return ret

    def clone(self):
        # assume default constructor exists
        obj = type(self)()
        mapper = inspect(self).mapper
        for prop in mapper.iterate_properties:
            if not isinstance(prop, ColumnProperty) or any([True for col in prop.columns if col.primary_key]):
                continue
            value = prop.class_attribute.__get__(self, None)
            prop.class_attribute.__set__(obj, value)
        return obj


#older attempt to json serialize base classes
#class AlchemyEncoder(json.JSONEncoder):
#    def default(self, o):
#        if hasattr(o, "as_dict") and callable(o.as_dict):
#            return json.JSONDecoder.default(self, o)
#        if isinstance(o.__class__, DeclarativeMeta):
#            # a SQLAlchemy class
#            fields = {}
#            for field in [x for x in dir(o) if not x.startswith('_') and x != 'metadata']:
#                data = o.__getattribute__(field)
#                try:
#                    json.dumps(data)
#                    fields[field] = data
#                except TypeError:
#                    fields[field] = None
#            return fields
#        return json.JSONDecoder.default(self, o)


class VersionCheckError(RuntimeError):
    pass