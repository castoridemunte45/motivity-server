import os
import random
import string
import sys
import transaction

from sqlalchemy import engine_from_config, func

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from pyramid.scripts.common import parse_vars

from ..models import (
    DBSession,
    Base,
    user,
    application
    )


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def create_schemas(engine):
    from sqlalchemy.engine import reflection
    from sqlalchemy.schema import CreateSchema

    table_names = Base.metadata.tables.keys()
    all_schemas = set([t.split('.')[0] for t in table_names if len(t.split('.')) > 1])
    inspector = reflection.Inspector.from_engine(engine)
    existing_schemas = inspector.get_schema_names()
    for s in all_schemas:
        if s not in existing_schemas:
            engine.execute(CreateSchema(s))


def drop_all(engine):
    Base.metadata.drop_all(engine)

    # from sqlalchemy.engine import reflection
    # from sqlalchemy.schema import DropSchema
    #
    # table_names = Base.metadata.tables.keys()
    # all_schemas = set([t.split('.')[0] for t in table_names if len(t.split('.')) > 1])
    # inspector = reflection.Inspector.from_engine(engine)
    # existing_schemas = inspector.get_schema_names()
    # for s in all_schemas:
    #     if s in existing_schemas:
    #         engine.execute(DropSchema(s))


def main(argv=sys.argv):
    if len(argv) < 2:
        usage(argv)
    config_uri = argv[1]
    options = parse_vars(argv[2:])

    setup_logging(config_uri)
    app_settings = get_appsettings(config_uri, options=options)
    engine = engine_from_config(app_settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    create_schemas(engine)
    if 'clear_tables' in options and options['clear_tables'] == 'True':
        drop_all(engine)
    Base.metadata.create_all(engine)

    from ..models.user import User, Achievement, UserAchievement, Friend
    from ..models.application import Mission, Activity
    from datetime import datetime, timedelta
    from random import randint

    with transaction.manager:
        exists = DBSession.query(User).first()
        if exists is None:
            DBSession.add(
                User(email='admin@motivity.com', first_name='admin', last_name='motivity', active=True, is_admin=True,
                     password='eb6d0d183a57f379d7cc8279008475d0e0df7395310ae22db557e4181a2854e1:a771d9611df2422d9d4189aa5f30f38e'))
            DBSession.add(User(email='user@motivity.com', first_name='user', last_name='motivity', active=True, is_admin=False,
                               password='eb6d0d183a57f379d7cc8279008475d0e0df7395310ae22db557e4181a2854e1:a771d9611df2422d9d4189aa5f30f38e'))
            DBSession.add(User(email='user@sync.com', first_name='user', last_name='sync', active=True, is_admin=False,
                               password='eb6d0d183a57f379d7cc8279008475d0e0df7395310ae22db557e4181a2854e1:a771d9611df2422d9d4189aa5f30f38e'))

            DBSession.add(Achievement(title='Rookie', description='Complete any 5 missions.', milestone=5, target='mission'))
            DBSession.add(Achievement(title='Experienced', description='Complete any 20 missions.', milestone=20, target='mission'))
            DBSession.add(Achievement(title='Veteran', description='Complete any 50 missions.', milestone=50, target='mission'))
            DBSession.add(Achievement(title='Master', description='Complete any 100 missions.', milestone=100, target='mission'))
            DBSession.add(Achievement(title='Guru', description='Complete any 500 missions.', milestone=500, target='mission'))
            DBSession.add(Achievement(title='One trick pony', description='Complete all missions of a certain activity.', milestone=1, target='activity'))
            DBSession.add(Achievement(title='Branching out', description='Complete all missions from 3 activities.', milestone=3, target='activity'))
            DBSession.add(Achievement(title='Master of all trades', description='Complete all missions from 5 activities.', milestone=5, target='activity'))
            DBSession.add(Achievement(title='Getting out of your shell', description='Make 5 friends.', milestone=5, target='user'))
            DBSession.add(Achievement(title='Things are better shared', description='Make 30 friends.', milestone=30, target='user'))
            DBSession.add(Achievement(title='Social butterfly', description='Make 100 friends.', milestone=100, target='user'))

            achievements = DBSession.query(Achievement).all()
            users = DBSession.query(User).all()
            for i in range(0, len(users)):
                for j in range(0, len(achievements)):
                    DBSession.add(UserAchievement(achievement_id=achievements[j].id, user_id=users[i].id,
                                                  progress=randint(0, int(achievements[j].milestone / 2))))

            # DBSession.add(Activity(title='Programming', description='Dream in code.', difficulty=4, user_id=1, next_level_exp=16))
            # DBSession.add(Activity(title='Cooking', description='Be your own chef.', difficulty=3, user_id=1, next_level_exp=8))
            # DBSession.add(Activity(title='Music', description='Get your groove on.', difficulty=2, user_id=1, next_level_exp=4))
            # DBSession.add(Activity(title='Sports', description='Stay in shape.', difficulty=2, user_id=1, next_level_exp=4))
            # DBSession.add(Activity(title='Reading', description='Let your imagination loose.', difficulty=3, user_id=1, next_level_exp=8))
            # DBSession.add(Activity(title='School', description='Get your things done.', difficulty=4, user_id=1, next_level_exp=16))

            for i in range(1, 4):
                DBSession.add(Activity(title='Programming', description='Dream in code.', difficulty=9, user_id=i,
                                       next_level_exp=16))
                DBSession.add(Activity(title='Cooking', description='Be your own chef.', difficulty=5, user_id=i,
                                       next_level_exp=8))
                DBSession.add(Activity(title='Music', description='Get your groove on.', difficulty=2, user_id=i,
                                       next_level_exp=4))
                DBSession.add(
                    Activity(title='Sports', description='Stay in shape.', difficulty=8, user_id=i, next_level_exp=4))
                DBSession.add(
                    Activity(title='Reading', description='Let your imagination loose.', difficulty=3, user_id=i,
                             next_level_exp=8))
                DBSession.add(Activity(title='School', description='Get your things done.', difficulty=4, user_id=i,
                                       next_level_exp=16))

            userObj = DBSession.query(User).first()

            missions_titles = ['Go', 'C#', 'Java', 'Brownies', 'Carrot Soup', 'Macaroni and Cheese',
                        'Learn to read sheet music.', 'Basic guitar cords', 'Play piano',
                        'Shoot 50 free-kicks.', 'Run 5 kilometers a week.', '10 push-ups every day',
                        'Finish a classic book.', 'Finish a non-fiction book.', 'Finish a SF book.',
                        'Help 3 colleagues with their homework.', 'Do 10 hours of community service.',
                        'Get on time to school.']

            missions_descriptions = ['Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.',
                                     'Is one of many .NET programming languages.',
                                     'Java is a general-purpose computer programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible.',
                                     'A brownie is a flat, baked dessert square that was developed in the United States at the end of the 19th century and popularized in the U.S. and Canada during the first half of the 20th century.',
                                     'Carrot soup is a soup prepared with carrot as a primary ingredient. It may be prepared as a cream- or broth-style soup. Additional vegetables, root vegetables and various other ingredients may be used in its preparation.',
                                     'Macaroni and cheese—also called mac and cheese in American, Canadian, and Australian English; macaroni pie in Caribbean English; and macaroni cheese in the United Kingdom—is a dish of English origin.',
                                     'At its very simplest, music is a language just like you’d read aloud from a book. The symbols you’ll see on pages of sheet music have been used for hundreds of years. And they represent the pitch, speed and rhythm of the song they convey, as well as expression and techniques used by a musician to play the piece. Think of the notes as the letters, the measures as the words, the phrases as the sentences and so forth. Learning to read music really does open up a whole new world to explore!', 'Basic guitar cords', 'Play piano',
                                     'he taking of a free kick occurs after a foul is committed. Many of the free kicks are taken directly where the shot is directed towards and into the goal without touching any player. Scoring of such direct free kicks takes great skills. Some of the finest exponents of this art are not all exactly among the greatest players of all times, but their contribution to the beautiful game just cannot be ignored. Some of their exploits are part of football folklore.', 'Run 5 kilometers a week', '10 push-ups every day',
                                     'A classic is a book accepted as being exemplary or noteworthy, for example through an imprimatur such as being listed in a list of great books, or through a reader \'s personal opinion. Although the term is often associated with the Western canon, it can be applied to works of literature from all traditions, such as the Chinese classics or the Indian Vedas',
                                     'Nonfiction or non-fiction is content (often, in the form of a story) whose creator, in good faith, assumes responsibility for the truth or accuracy of the events, people, and/or information presented. In contrast, a story whose creator explicitly leaves open if and how the work refers to reality is usually classified as fiction.',
                                     'Science fiction (often shortened to sci-fi or scifi) is a genre of speculative fiction, typically dealing with imaginative concepts such as futuristic science and technology, space travel, time travel, faster than light travel, parallel universes, and extraterrestrial life. Science fiction often explores the potential consequences of scientific and other innovations, and has been called a "literature of ideas." ',
                                     'Try to be a good colleague', 'Do 10 hours of community service.',
                                     'Try to be a good student and on time at school.']

            for i in range(0, len(missions_titles)):
                DBSession.add(Mission(title=missions_titles[i], description=missions_descriptions[i], duration=randint(101, 1000), progress=0,
                                      milestone=randint(5, 100), created_at=func.now() + timedelta(hours=-i)))

            activities = DBSession.query(Activity).all()
            for activity_id in range(0, len(activities)):
                activity = DBSession.query(Activity).filter_by(id=activity_id + 1).first();
                for i in range(((3 * (activity.id - 1)) + 1), ((3 * activity.id)) + 1):
                    mission = DBSession.query(Mission).filter_by(id=i).first()
                    if mission is not None:
                        mission.activities.append(activity)

            missions = DBSession.query(Mission).all()
            for i in range(0, len(missions)):
                if i % 2 == 0:
                    mission = DBSession.query(Mission).filter_by(id=i+1).first()
                    userObj.missions.append(mission)

            userObj = DBSession.query(User).filter_by(id=2).first()
            for i in range(0, len(missions)):
                if i % 2 == 1:
                    mission = DBSession.query(Mission).filter_by(id=i+1).first()
                    userObj.missions.append(mission)

            DBSession.add(Friend(user_id=userObj.id, friend_id=1, nickname='Castor'))