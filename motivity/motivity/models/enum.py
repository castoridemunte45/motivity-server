import re

from sqlalchemy.types import SchemaType, TypeDecorator, Enum as SqlEnum
from sqlalchemy.util import set_creation_order, OrderedDict

class EnumSymbol(object):
    """Define a fixed symbol tied to a parent class."""

    def __init__(self, value, description=None):
        self.value = value
        self.description = description
        set_creation_order(self)

    def bind(self, cls, name):
        """Bind symbol to a parent class."""
        self.cls = cls
        self.name = name
        setattr(cls, name, self)

    def __reduce__(self):
        """Allow unpickling to return the symbol linked to the DeclEnum class."""
        return getattr, (self.cls, self.name)

    def __iter__(self):
        return iter([self.value, self.description])

    def __repr__(self):
        return "<%s>" % self.name


class DeclEnumMeta(type):
    """Generate new DeclEnum classes."""

    def __init__(cls, classname, bases, dict_):
        reg = cls._reg = cls._reg.copy()
        for k in sorted(dict_):
            if k.startswith('__'):
                continue
            v = dict_[k]
            if isinstance(v, str):
                v = EnumSymbol(v)
            elif isinstance(v, tuple) and len(v) == 2:
                v = EnumSymbol(*v)
            if isinstance(v, EnumSymbol):
                v.bind(cls, k)
                reg[k] = v
        reg.sort(key=lambda k: reg[k]._creation_order)
        return type.__init__(cls, classname, bases, dict_)

    def __iter__(cls):
        return iter(cls._reg.values())

    def __getitem__(cls, index):
        return list(cls.__iter__())[index]

    def __len__(cls):
        return len(list(cls.__iter__()))

class DeclEnum(metaclass=DeclEnumMeta):
    """Declarative enumeration.

    Attributes can be strings (used as values),
    or tuples (used as value, description) or EnumSymbols.
    If strings or tuples are used, order will be alphabetic,
    otherwise order will be as in the declaration.

    """

    __metaclass__ = DeclEnumMeta
    _reg = OrderedDict()
    _json_use_values = False

    @classmethod
    def json_names(cls):
        if cls._json_use_values:
            return [i.value for i in cls._reg.values()]
        return cls._reg.keys()

    @classmethod
    def from_string(cls, name):
        if name in cls._reg:
            return cls._reg[name]
        for val in cls._reg.values():
            if val.value == name:
                return val
        raise KeyError(name)

    @classmethod
    def db_type(cls, **kwargs):
        return DeclEnumType(cls, **kwargs)


class DeclEnumType(SchemaType, TypeDecorator):
    """DeclEnum augmented so that it can persist to the database."""

    def __init__(self, enum, name=None, **kwargs):
        self.enum = enum
        if hasattr(enum, "__db_name") and name is None:
            name = getattr(enum, "__db_name")
        if hasattr(enum, "__db_schema") and 'schema' not in kwargs:
            kwargs['schema'] = getattr(enum, "__db_schema")
        if name is None:
            name = "ck%s" % re.sub('([A-Z])', lambda m: '_' + m.group(1).lower(), enum.__name__)
        kwargs['name'] = name
        self.impl = SqlEnum(*enum._reg.keys(), **kwargs)

    @property
    def python_type(self):
        return self.enum

    def _set_table(self, table, column):
        self.impl._set_table(table, column)

    def copy(self):
        return DeclEnumType(self.enum)

    def process_bind_param(self, value, dialect):
        if isinstance(value, EnumSymbol):
            value = value.name
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            return getattr(self.enum, value.strip())


def EnumAttrs(name, schema=None, json_use_values=False):
    def f(cls):
        if schema is not None:
            cls.__db_schema = schema
        cls.__db_name = name
        cls._json_use_values = json_use_values
        return cls
    return f


class PythonEnum(TypeDecorator, SchemaType):
    impl = SqlEnum
    def __init__(self, enum_class, **kw):
        if hasattr(enum_class, "__db_name") and 'name' not in kw:
            kw['name'] = getattr(enum_class, "__db_name")
        if hasattr(enum_class, "__db_schema") and 'schema' not in kw:
            kw['schema'] = getattr(enum_class, "__db_schema")
        if 'name' not in kw:
            kw['name'] = "ck%s" % re.sub('([A-Z])', lambda m: '_' + m.group(1).lower(), enum_class.__name__)
        #self.impl = SqlEnum(*(m.name for m in enum_class), **kw)
        super().__init__(*(m.name for m in enum_class), **kw)
        self._enum_class = enum_class

    def process_bind_param(self, value, dialect):
        if isinstance(value, self._enum_class):
            return value.name
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            return self._enum_class[value]

    @property
    def python_type(self):
        return self._enum_class

    def copy(self):
        return PythonEnum(self._enum_class)

    def _set_table(self, table, column):
        self.impl._set_table(table, column)