from sqlalchemy import Column, ForeignKey, Table, PrimaryKeyConstraint,DateTime, func, UniqueConstraint
from sqlalchemy.orm import relationship, backref

from ..json import DictSerializable
from . import Base, types
from .enum import PythonEnum
from .application import Mission, Activity


class User(Base, DictSerializable):
    __tablename__ = 'users'
    __table_args__ = dict(schema='user')

    id = Column(types.Id, primary_key=True)
    email = Column(types.String, nullable=False, unique=True)
    first_name = Column(types.String, default="")
    last_name = Column(types.String, nullable=False, default="")
    password = Column(types.Password, nullable=False)
    is_admin = Column(types.Boolean, nullable=False, default=False)
    seen_tutorial = Column(types.Boolean, nullable=False, default=False)
    active = Column(types.Boolean, nullable=False, default=True)
    experience = Column(types.Integer, default=0)
    currency = Column(types.Integer, default=0)

    # mission_ids = Column(types.ARRAY(types.Integer), default=[])


class Friend(Base, DictSerializable):
    __tablename__ = 'friends'
    __table_args__ = dict(schema='user')

    id = Column(types.Id, primary_key=True)
    user_id = Column(types.Id, ForeignKey(User.id, ondelete='CASCADE', onupdate='CASCADE'))
    friend_id = Column(types.Id, ForeignKey(User.id, ondelete='CASCADE', onupdate='CASCADE'))
    nickname = Column(types.String, default="")

    user = relationship(User, backref=backref("friends", passive_deletes=True, cascade="all, delete-orphan"),
                        foreign_keys=user_id)
    friend = relationship(User, foreign_keys=friend_id)

    __table_args__ = (UniqueConstraint(user_id, friend_id),
                      dict(schema='user'))


class Achievement(Base, DictSerializable):
    __tablename__ = 'achievements'
    __table_args__ = dict(schema='user')

    id = Column(types.Id, primary_key=True)
    title = Column(types.String, nullable=False)
    description = Column(types.String, default="")
    milestone = Column(types.Integer, nullable=False, default=0)
    target = Column(PythonEnum(types.Targets), nullable=False)


class UserAchievement(Base, DictSerializable):
    __tablename__ = 'user_achievements'
    __table_args__ = dict(schema='user')

    id = Column(types.Id, primary_key=True)
    user_id = Column(types.Id, ForeignKey(User.id, ondelete='CASCADE', onupdate='CASCADE'))
    achievement_id = Column(types.Id, ForeignKey(Achievement.id, ondelete='CASCADE', onupdate='CASCADE'))
    completed = Column(types.Boolean, default=False)
    progress = Column(types.Integer, nullable=False, default=0)
    date_acquired = Column(DateTime(timezone=True), default=func.now(), nullable=False)

    user = relationship(User, backref=backref("user_achievements", passive_deletes=True, cascade="all, delete-orphan"))
    achievement = relationship(Achievement, backref=backref("user_achievements", passive_deletes=True, cascade="all, delete-orphan"))

# this should probably not be here
# user_missions = Table(
#     'user_missions', Base.metadata,
#     Column("user_id", ForeignKey(User.id, ondelete="CASCADE", onupdate="CASCADE")),
#     Column("mission_id", ForeignKey(Mission.id, ondelete="CASCADE", onupdate="CASCADE")),
#     PrimaryKeyConstraint('user_id', 'mission_id'),
#     schema="user"
# )
# User.missions = relationship(Mission, secondary=user_missions, backref=backref('users'))

# this should probably not be here
# user_activities = Table(
#     'user_activities', Base.metadata,
#     Column("user_id", ForeignKey(User.id, ondelete="CASCADE", onupdate="CASCADE")),
#     Column("activity_id", ForeignKey(Activity.id, ondelete="CASCADE", onupdate="CASCADE")),
#     PrimaryKeyConstraint('user_id', 'activity_id'),
#     schema="user"
# )
# User.activites = relationship(Activity, secondary=user_activities, backref=backref('users'))
