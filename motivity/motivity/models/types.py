from decimal import Decimal

from sqlalchemy import String, Text, Numeric, Integer, DateTime, Boolean
from sqlalchemy.dialects.postgresql import ARRAY
from .enum import EnumAttrs
from enum import Enum

Id = Integer
Number = Integer
Label = String(100)
Code = String(10)
LongCode = String(20)
Description = String(500)
LongDescription = Text
Password = String(120)
Email = String(100)
Phone = String(25)
Rate = Numeric


@EnumAttrs("targets", "user")
class Targets(Enum):
    user = "user"
    activity = "activity"
    mission = "mission"
