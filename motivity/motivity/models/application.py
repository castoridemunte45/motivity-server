from sqlalchemy import Column, ForeignKey, Table, PrimaryKeyConstraint, func
from sqlalchemy.orm import relationship, backref

from ..json import DictSerializable
from . import Base, types


class Activity(Base, DictSerializable):
    __tablename__ = 'activities'
    __table_args__ = dict(schema='application')

    id = Column(types.Id, primary_key=True)
    user_id = Column(types.Id, ForeignKey('user.users.id', ondelete='CASCADE', onupdate='CASCADE'))
    title = Column(types.String, nullable=False)
    description = Column(types.String, default="")
    difficulty = Column(types.Integer, nullable=False, default=1)
    experience = Column(types.Integer, nullable=False, default=0)
    next_level_exp = Column(types.Integer, nullable=False, default=0)
    level = Column(types.Integer, nullable=False, default=1)

    user = relationship('User', backref=backref("activities", passive_deletes=True, cascade="all, delete-orphan"))


class Mission(Base, DictSerializable):
    __tablename__ = 'missions'
    __table_args__ = dict(schema='application')

    id = Column(types.Id, primary_key=True)
    user_id = Column(types.Id, ForeignKey("user.users.id", ondelete='CASCADE', onupdate='CASCADE'))
    title = Column(types.String, nullable=False)
    duration = Column(types.Integer, nullable=False, default=0)
    description = Column(types.String, default="")
    completed = Column(types.Boolean, default=False)
    progress = Column(types.Integer, nullable=False, default=0)
    milestone = Column(types.Integer)
    created_at = Column(types.DateTime(timezone=True), nullable=False, default=func.now(), server_default=func.now())

    user = relationship("User", backref=backref("missions", passive_deletes=True, cascade="all, delete-orphan"))

mission_activities = Table(
    'mission_activities', Base.metadata,
    Column("mission_id", ForeignKey(Mission.id, ondelete="CASCADE", onupdate="CASCADE")),
    Column("activity_id", ForeignKey(Activity.id, ondelete="CASCADE", onupdate="CASCADE")),
    PrimaryKeyConstraint('mission_id', 'activity_id'),
    schema="application"
)
Mission.activities = relationship(Activity, secondary=mission_activities, backref=backref('mission'))

